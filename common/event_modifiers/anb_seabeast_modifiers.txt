sea_beast_kraken = {
	is_galleon_modifier = yes
	hull_size_modifier = 2
	engagement_cost = 1
	heavy_ship_power = 0.66
	naval_morale_damage = 0.25
}
